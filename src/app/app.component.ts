import { Component } from '@angular/core';

const maxDigitos = 9;
const duracionLlamada = 5;

@Component({
  selector: 'telf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public numero = '';
  public llamando = false;
  public numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  private timer;

  public esNumeroCompleto(): boolean {
    return this.numero.length === maxDigitos;
  }

  public pulsaNumero(n: number): void {
    if (!this.esNumeroCompleto()) {
      this.numero += n;
    }
  }

  public borrarNumero(): void {
    this.numero = '';
  }

  public handlerLlamada(evento: Event): void {
    evento.preventDefault();
    if (this.llamando) {
      this.colgar();
    } else {
      this.llamar();
    }
  }

  public colgar() {
    this.borrarNumero();
    clearTimeout(this.timer);
    this.llamando = false;
  }

  public llamar() {
    if (!this.esNumeroCompleto()) {
      return false;
    }
    this.timer = setTimeout(() => {
      this.colgar();
    }, duracionLlamada * 1000);
    this.llamando = true;
  }
}
